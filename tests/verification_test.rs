#[cfg(test)]
use rand::RngCore;
use lib_zk_battleship::verification::*;
use lib_zk_battleship::board::{Board, CommitableBoard};
use lib_zk_battleship::salt::Salter;

use rand::rngs::StdRng;
use rand::SeedableRng;

const X: bool = true;
const O: bool = false;
fn example_ok_board() -> Board {
    return Board::new([
        [X, X, X, X, O, O, O, O, X, O],
        [O, O, O, O, O, O, X, O, O, O],
        [O, O, O, X, X, O, O, O, O, O],
        [O, O, O, O, O, O, O, O, O, O],
        [O, X, O, X, O, O, O, O, O, O],
        [O, O, O, X, O, O, O, O, O, O],
        [O, O, O, X, O, O, O, O, O, O],
        [O, O, O, O, O, O, O, O, X, X],
        [O, X, X, X, O, O, O, O, O, O],
        [O, O, O, O, O, X, O, O, X, X],
    ]);
}

fn example_ok_commitable_board(rng: &mut dyn RngCore) -> CommitableBoard {
    return CommitableBoard::from_board(&example_ok_board(), rng).unwrap();
}

#[test]
fn test_correct_baord_pass_all_verifications() {
    let mut rng = StdRng::seed_from_u64(42);
    let board = example_ok_commitable_board(&mut rng);
    let mut salt = Salter::new(&mut rng);
    let commitment = salt.create_commitment(&board);

    let mut queries = vec![
        VerifyingQuery::CorrectBoard(),
        VerifyingQuery::AllShipsPlaced(),
        VerifyingQuery::Play(),
    ];

    for i in 1..11 {
        queries.push(VerifyingQuery::CorrectShipPlacement(i));
    }

    for query in queries {
        print!("{:?}\n", query);
        let answer = get_answer(&board, &salt, query);
        assert!(verify_answer(query, answer, &commitment));
    }
}

#[test]
fn test_inccorect_board_duplicate_position() {
    let mut rng = StdRng::seed_from_u64(42);
    let mut board = example_ok_commitable_board(&mut rng);
    let mut salt = Salter::new(&mut rng);
    board.shuffled_cells[0].position = board.shuffled_cells[1].position;
    let commitment = salt.create_commitment(&board);

    let query = VerifyingQuery::CorrectBoard();

    let answer = get_answer(&board, &salt, query);
    assert!(!verify_answer(query, answer, &commitment));
}

#[test]
fn test_inccorect_board_too_many_ships() {
    let mut rng = StdRng::seed_from_u64(42);
    let mut board = example_ok_commitable_board(&mut rng);
    let mut salt = Salter::new(&mut rng);
    board.shuffled_cells[0].ship_id = Some(board.shuffled_cells[0].ship_id.unwrap() + 1);

    let commitment = salt.create_commitment(&board);

    let query = VerifyingQuery::AllShipsPlaced();

    let answer = get_answer(&board, &salt, query);
    assert!(!verify_answer(query, answer, &commitment));
}

#[test]
fn test_inccorect_board_wrong_ship() {
    let mut rng = StdRng::seed_from_u64(42);
    let mut board = example_ok_commitable_board(&mut rng);
    let mut salt = Salter::new(&mut rng);
    let t = board.shuffled_cells[0].ship_id.unwrap();
    board.shuffled_cells[0].ship_id = Some(t + 1);

    let commitment = salt.create_commitment(&board);

    for id in t..t + 2 {
        if id > 0 && id <= 10 {
            let query = VerifyingQuery::CorrectShipPlacement(id);

            let answer = get_answer(&board, &salt, query);
            assert!(!verify_answer(query, answer, &commitment));
        }
    }
}

#[test]
fn test_inccorect_board_wrong_trans() {
    let mut rng = StdRng::seed_from_u64(42);
    let mut board = example_ok_commitable_board(&mut rng);
    let mut salt = Salter::new(&mut rng);
    let commitment = salt.create_commitment(&board);

    board.t.swaps_xy = !board.t.swaps_xy;
    let query = VerifyingQuery::Play();
    let answer = get_answer(&board, &salt, query);
    assert!(!verify_answer(query, answer, &commitment));
}
