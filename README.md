# README #

Fast Zero-Knowledge Battleship game implementation in Rust

### Contribution guidelines ###
* Building - `cargo build`
* Running tests - `cargo test`
* Unit tests are placed in the same file as the code they test, in `src/`
* Integration tests are located in `tests/`

### Implementation details ###
Game Board is represented as 10x10, zero indexed, two-dimensional array of bools, where true means that given cell belongs to
some ship. To create commitments, Board is converted to `CommitableBoard`, which contains transformation and `Cell`s with data used to create commitments:
* ship position after random transformation of board,
* ship id, 
* flags v1, v2, v3, v4 telling if a given cell is a part of ship of length at least 1,2,3,4, respectively.

`CommitableBoard` can be later converted to `Commitment` by `Salter`, which is responsible for generating nonces and hash all required fields.
When other player asks to answer query of some type, player has to send that `CommitableBoard`, revealing only salts necessary to answer
that question. Because of that, `CellSalt` and `Salt` have all fields optional.
To keep `CellSalt` consistent with `Cell`, the latter also has optional fields -- either both salt and field corresponding to that salt should be `None` or `Some`.

All verification queries and answers to them are handled in `verification.rs` file. Converting Board to `CommitableBoard` also requires 
some basic verification, so `from_board` function only performs very simple validation required to correctly parse board. 
Apart from verifying opponent's board, the game by default verifies player's board to make sure he or she didn't lose by
mistake in verification phase. 
This can be changed by modifying `STRICT_VERIFICATION_AT_PLACEMENT_PHASE` flag in `config.rs` - one can check that any such attempt to cheat (eg. not enough ships placed, two ships sharing 
a corner) will be caught with very high probability in the verification phase. Other cheating methods, like placing too long ship, ships with non-line shape
wouldn't parse to commitable board even with turned off `STRICT_VERIFICATION_AT_PLACEMENT_PHASE`, but if one 
tried to cheat using such method, this of course would also be caught.

For communication over internet, the TCP/IP protocol is used. Relevant code is located in `connection.rs`.

As frontend, we use `tui` - the terminal UI. Implementation details can be found in `rendering.rs` and `main.rs`.