pub type Coordinate = u32;

use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct TransformationModulo<const MODULO: u32> {
    pub v: (Coordinate, Coordinate),
    pub swaps_xy: bool,
}

impl<const MODULO: u32> TransformationModulo<MODULO> {
    pub fn transform(self: &Self, position: (Coordinate, Coordinate)) -> (Coordinate, Coordinate) {
        let position = (
            (position.0 + self.v.0) % MODULO,
            (position.1 + self.v.1) % MODULO,
        );
        return match self.swaps_xy {
            false => position,
            true => (position.1, position.0),
        };
    }
    pub fn inverse_transform(
        self: &Self,
        position_after: (Coordinate, Coordinate),
    ) -> (Coordinate, Coordinate) {
        let position_after = match self.swaps_xy {
            false => position_after,
            true => (position_after.1, position_after.0),
        };
        return (
            (position_after.0 + MODULO - self.v.0).rem_euclid(MODULO),
            (position_after.1 + MODULO - self.v.1).rem_euclid(MODULO),
        );
    }

    pub fn to_byte_array(self: &Self) -> [u8; 12] {
        return [
            self.v.0.to_le_bytes(),
            self.v.1.to_le_bytes(),
            (self.swaps_xy as u32).to_le_bytes(),
        ]
        .concat()
        .try_into()
        .unwrap();
    }

    pub fn get_neighbours(position: (Coordinate, Coordinate)) -> Vec<(Coordinate, Coordinate)> {
        const DIRECTIONS: [(i32, i32); 8] = [
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0),
            (1, 1),
            (1, -1),
            (-1, 1),
            (-1, -1),
        ];

        return DIRECTIONS
            .iter()
            .map(|v| {
                (
                    (position.0 as i32 + v.0).rem_euclid(MODULO as i32) as u32,
                    (position.1 as i32 + v.1).rem_euclid(MODULO as i32) as u32,
                )
            })
            .collect::<Vec<(Coordinate, Coordinate)>>()
    }

    // sprawdza czy podane koordynaty tworzą prostą linię
    pub fn is_line(positions: &Vec<(Coordinate, Coordinate)>) -> bool {
        const DIRECTIONS: [(i32, i32); 2] = [(0, 1), (1, 0)];

        for position in positions {
            for v in DIRECTIONS {
                // dla kazdej pozycji p i przesuniecia v sprawdzam czy X = (p,p+v,p+2*v...) odpowiada koordynatom na wejsciu
                let ok = (0..positions.len() as i32)
                    .map(|k| {
                        (
                            (position.0 as i32 + v.0 * k).rem_euclid(MODULO as i32) as u32,
                            (position.1 as i32 + v.1 * k).rem_euclid(MODULO as i32) as u32,
                        )
                    })
                    .all(|t| positions.contains(&t));

                if ok {
                    return true;
                }
            }
        }
        return false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    type Transformation14 = TransformationModulo<14>;

    #[test]
    fn test_transform_no_swap() {
        let pos = (1, 6);
        let t = Transformation14 {
            v: (3, 13),
            swaps_xy: false,
        };
        assert_eq!(t.transform(pos), (4, 5));
    }

    #[test]
    fn test_inverse_transform_no_swap() {
        let pos = (4, 5);
        let t = Transformation14 {
            v: (3, 13),
            swaps_xy: false,
        };
        assert_eq!(t.inverse_transform(pos), (1, 6));
    }

    #[test]
    fn test_transform_swaps() {
        let pos = (1, 6);
        let t = Transformation14 {
            v: (3, 13),
            swaps_xy: true,
        };
        assert_eq!(t.transform(pos), (5, 4));
    }

    #[test]
    fn test_inverse_transform_swaps() {
        let pos = (5, 4);
        let t = Transformation14 {
            v: (3, 13),
            swaps_xy: true,
        };
        assert_eq!(t.inverse_transform(pos), (1, 6));
    }

    #[test]
    fn test_get_neighbours() {
        let pos = (13, 0);
        let mut neighbours = Transformation14::get_neighbours(pos);
        let mut excpected = vec![
            (12, 12),
            (13, 12),
            (0, 12),
            (12, 0),
            (13, 0),
            (0, 0),
            (12, 1),
            (13, 1),
            (0, 1),
        ];
        assert_eq!(neighbours.sort(), excpected.sort());
    }

    #[test]
    fn is_line_ok1() {
        let pos = vec![(13, 0), (13, 1), (13, 13), (13, 12)];
        assert!(Transformation14::is_line(&pos));
    }

    #[test]
    fn is_lineok_2() {
        let pos = vec![(0, 0), (0, 1), (0, 2)];
        assert!(Transformation14::is_line(&pos));
    }

    #[test]
    fn is_line_wrong1() {
        let pos = vec![(0, 0), (0, 0), (0, 2)];
        assert!(!Transformation14::is_line(&pos));
    }

    #[test]
    fn is_line_ok3() {
        let pos = vec![(3, 3)];
        assert!(Transformation14::is_line(&pos));
    }

    #[test]
    fn is_line_wrong2() {
        let pos = vec![(3, 3), (2, 2)];
        assert!(!Transformation14::is_line(&pos));
    }

    #[test]
    fn test_to_byte_array() {
        let t = Transformation14 {
            v: (3, 10),
            swaps_xy: true,
        };
        assert_eq!(t.to_byte_array(), [3, 0, 0, 0, 10, 0, 0, 0, 1, 0, 0, 0]);
    }
}
