use crate::board::*;
use crate::hash::{hash_transformation, Commitment};
use crate::salt::*;
use crate::transformation::*;
use crate::verification_common::*;
use itertools::Itertools;
use multiset::HashMultiSet;
use std::collections::HashSet;

use crate::config::*;

use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum VerifyingQuery {
    CorrectBoard(),
    CorrectShipPlacement(ShipID),
    AllShipsPlaced(),
    Play(),
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Answer {
    OK((Transformation, Salt)),
    Cells(Vec<(usize, Cell, CellSalt)>),
    CellsAndTransformation(Vec<(usize, Cell, CellSalt)>, (Transformation, Salt)),
}

fn get_answer_correct_board_single(
    idx: usize,
    board: &CommitableBoard,
    salter: &Salter,
) -> (usize, Cell, CellSalt) {
    // chce wyslac tylko informacje o pozycji
    let mut cell = get_empty_cell();
    cell.position = board.get_cell(idx).position;

    let cell_salt: CellSalt = [
        None,
        None,
        None,
        None,
        None,
        salter.get_cell_salt(idx)[CELL_SALT_SHIP_POSITION],
    ];

    return (idx, cell, cell_salt);
}

fn get_answer_correct_board(board: &CommitableBoard, salter: &Salter) -> Answer {
    let answer = (0..COMMITABLE_BOARD_TOTAL_CELLS)
        .map(|i| get_answer_correct_board_single(i as usize, &board, &salter))
        .collect();

    return Answer::CellsAndTransformation(
        answer,
        (board.get_transformation(), salter.get_transformation_salt()),
    );
}

fn all_placed_single(
    idx: usize,
    board: &CommitableBoard,
    salter: &Salter,
) -> (usize, Cell, CellSalt) {
    // chce usunac tylko informacje o pozycji
    let mut cell: Cell = board.get_cell(idx).clone();
    cell.position = None;

    // usuwam tylko sol dla pozycji
    let mut cell_salt: CellSalt = salter.get_cell_salt(idx).clone();
    cell_salt[CELL_SALT_SHIP_POSITION] = None;

    return (idx, cell, cell_salt);
}

fn get_answer_all_placed(board: &CommitableBoard, salter: &Salter) -> Answer {
    let answer = (0..COMMITABLE_BOARD_TOTAL_CELLS)
        .map(|i| all_placed_single(i as usize, &board, &salter))
        .collect();

    return Answer::Cells(answer);
}

fn get_answer_ship_placement(ship_id: ShipID, board: &CommitableBoard, salter: &Salter) -> Answer {
    let ship_coordinates: Vec<(Coordinate, Coordinate)> = (0..COMMITABLE_BOARD_TOTAL_CELLS)
        .filter(|i| board.get_cell(*i as usize).ship_id == Some(ship_id))
        .map(|i| board.get_cell(i as usize).position.unwrap())
        .collect();

    // coordynaty wszystkich pol statku oraz sasiadnich
    let mut neighbours = HashSet::new();

    for v in ship_coordinates {
        neighbours.insert(v.clone());
        Transformation::get_neighbours(v).iter_mut().for_each(|c| {
            neighbours.insert(c.clone());
        });
    }

    let result = (0..COMMITABLE_BOARD_TOTAL_CELLS)
        // biore tylko te które są sąsiadami statku
        .filter(|i| neighbours.contains(&board.get_cell(*i as usize).position.unwrap()))
        // Nie ukrywam żadnych informacji, wysyłam caly cell i caly salt
        .map(|i| {
            (
                i as usize,
                board.get_cell(i as usize).clone(),
                salter.get_cell_salt(i as usize).clone(),
            )
        })
        .collect();

    return Answer::Cells(result);
}

fn verify_hash_transformation(
    transformation: Transformation,
    salt: Salt,
    commitment: &Commitment,
) -> bool {
    return hash_transformation(transformation, salt) == commitment.0;
}

// sprawdza czy wektor jest legalny, tj czy zawiera różne wartości, oraz czy dla kazdeg tupla zgadzaja sie pozycje Nones
fn verify_legal_cells(cells: &Vec<(usize, Cell, CellSalt)>) -> bool {
    let h: HashSet<usize> = cells.iter().map(|cell| cell.0).collect();
    return h.len() == cells.len() && cells.iter().all(|cell| verify_same_nones(cell.1, cell.2));
}

// weryfikuje czy pojedynczy cell dobrze opisuje statek
fn verify_correct_ship_info(cell: Cell) -> bool {
    // sprawdzam czy sa informacje o rozmiarze statku
    for i in 0..4 {
        if cell.ship_size[i].is_none() {
            return false;
        }
    }

    // sprawdzam czy sa informacje o id statku i czy sa poprawne
    if cell.ship_id.is_none() || cell.ship_id.unwrap() >= SHIP_SIZE.len() as u8 {
        return false;
    }

    // sprawdzam czy ship_size zawiera poprawne kodowanie statku
    return (0..4)
        .all(|i| cell.ship_size[i].unwrap() == (SHIP_SIZE[cell.ship_id.unwrap() as usize] > i));
}

fn verify_correct_board(
    cells: Vec<(usize, Cell, CellSalt)>,
    transformation: (Transformation, Salt),
    commitment: &Commitment,
) -> bool {
    // sprawdzam czy dostalem odpowiednio duzo wartosci
    if cells.len() != COMMITABLE_BOARD_TOTAL_CELLS as usize || !verify_legal_cells(&cells) {
        return false;
    }

    verify_hash_transformation(transformation.0, transformation.1, &commitment);

    let mut positions = HashSet::new();

    for cell in &cells {
        // jezeli brakuje ktorejs z wartosci lub nie zgadza się hash zwracam false
        if cell.1.position.is_none() || !verify_hash_cell(cell.0, cell.1, cell.2, &commitment) {
            return false;
        }

        // jezeli wspolrzedne sa spoza zakresu
        if cell.1.position.unwrap().1 >= TRANSFORMATION_MODULO
            || cell.1.position.unwrap().0 >= TRANSFORMATION_MODULO
        {
            return false;
        }

        positions.insert(transformation.0.inverse_transform(cell.1.position.unwrap()));
    }

    // sprawdzam czy wszystkie pola znajdują się w secie
    return (0..COMMITABLE_BOARD_SIDE_LEN)
        .cartesian_product(0..COMMITABLE_BOARD_SIDE_LEN)
        .all(|(i, j)| positions.contains(&(i, j)));
}

fn verify_all_placed(cells: Vec<(usize, Cell, CellSalt)>, commitment: &Commitment) -> bool {
    // sprawdzam czy dosalem odpowiednio duzo wartosci
    if cells.len() != COMMITABLE_BOARD_TOTAL_CELLS as usize || !verify_legal_cells(&cells) {
        return false;
    }

    let mut ships = HashMultiSet::new();
    for cell in cells {
        if !verify_correct_ship_info(cell.1)
            || !verify_hash_cell(cell.0, cell.1, cell.2, commitment)
        {
            return false;
        }
        // wrzucam id statku
        ships.insert(cell.1.ship_id.unwrap());
    }

    // sprawdzam czy liczba pol o danym ship_id odpowiada wielkosci statku
    return (1..SHIP_SIZE.len()).all(|i| ships.count_of(&(i as u8)) == SHIP_SIZE[i]);
}

fn verify_correct_ship_placement(
    ship_id: ShipID,
    cells: Vec<(usize, Cell, CellSalt)>,
    commitment: &Commitment,
) -> bool {
    if !verify_legal_cells(&cells) {
        return false;
    }

    let ship_size = SHIP_SIZE[ship_id as usize];

    // statek o rozmiarze k wraz z sasiadami zajmuje 3k+6 pol
    if cells.len() != 3 * ship_size + 6 {
        return false;
    }

    let mut all_coordinates = HashSet::new();

    for cell in &cells {
        if !verify_correct_ship_info(cell.1) // czy dobrze opisuje statek
            || cell.1.position.is_none()  //czy zawiera koordynaty
            || !verify_hash_cell(cell.0, cell.1, cell.2, commitment) //czy zgadza sie hash
            || (cell.1.ship_id.unwrap() != ship_id && cell.1.ship_id.unwrap() != 0)
        // czy zgadza sie id
        {
            return false;
        }
        all_coordinates.insert(cell.1.position.unwrap());
    }

    let ships: Vec<(Coordinate, Coordinate)> = cells
        .clone()
        .iter()
        .filter(|cell| cell.1.ship_id.unwrap() == ship_id)
        .map(|cell| cell.1.position.unwrap())
        .collect();

    // sprawdzam czy  zgadza sie rozmiar i statek  tworzy linie
    if ships.len() != ship_size || !Transformation::is_line(&ships) {
        return false;
    }

    // sprawdzam czy wszyscy sasiedzi statku sa w wektorze
    for ship in ships {
        let neighbours = Transformation::get_neighbours(ship);
        let ok = neighbours.iter().all(|n| all_coordinates.contains(&n));
        if !ok {
            return false;
        }
    }

    return true;
}

pub fn get_answer(board: &CommitableBoard, salter: &Salter, query: VerifyingQuery) -> Answer {
    match query {
        VerifyingQuery::CorrectBoard() => {
            return get_answer_correct_board(board, salter);
        }
        VerifyingQuery::CorrectShipPlacement(ship_id) => {
            return get_answer_ship_placement(ship_id, board, salter);
        }
        VerifyingQuery::AllShipsPlaced() => {
            return get_answer_all_placed(board, salter);
        }
        VerifyingQuery::Play() => {
            return Answer::OK((board.get_transformation(), salter.get_transformation_salt()));
        }
    }
}

pub fn verify_answer(query: VerifyingQuery, answer: Answer, commitment: &Commitment) -> bool {
    match query {
        VerifyingQuery::CorrectBoard() => match answer {
            Answer::CellsAndTransformation(cells, transformation) => {
                return verify_correct_board(cells, transformation, commitment);
            }
            _ => {
                return false;
            }
        },

        VerifyingQuery::CorrectShipPlacement(ship_id) => match answer {
            Answer::Cells(cells) => {
                return verify_correct_ship_placement(ship_id, cells, commitment);
            }
            _ => {
                return false;
            }
        },
        VerifyingQuery::AllShipsPlaced() => match answer {
            Answer::Cells(cells) => {
                return verify_all_placed(cells, commitment);
            }
            _ => {
                return false;
            }
        },

        VerifyingQuery::Play() => match answer {
            Answer::OK(transformation) => {
                return verify_hash_transformation(transformation.0, transformation.1, commitment);
            }
            _ => {
                return false;
            }
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::rngs::StdRng;
    use rand::SeedableRng;
    type Transformation14 = TransformationModulo<14>;

    fn empty_commitable_board() -> CommitableBoard {
        let mut shuffled_cells: [Cell; COMMITABLE_BOARD_TOTAL_CELLS as usize] =
            [get_empty_cell(); COMMITABLE_BOARD_TOTAL_CELLS as usize];

        for i in 0..COMMITABLE_BOARD_TOTAL_CELLS {
            let x = i % COMMITABLE_BOARD_SIDE_LEN;
            let y = i / COMMITABLE_BOARD_SIDE_LEN;
            shuffled_cells[i as usize] = Cell {
                ship_size: [Some(false), Some(false), Some(false), Some(false)],
                ship_id: Some(0),
                position: Some((x + 1, y + 1)),
            };
        }

        return CommitableBoard {
            t: Transformation14 {
                v: (1, 1),
                swaps_xy: false,
            },
            shuffled_cells: shuffled_cells,
        };
    }

    #[test]
    fn test_get_answer_correct_board() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rng);

        let answer = get_answer_correct_board(&board, &salt);

        match answer {
            Answer::CellsAndTransformation(cells, transformation) => {
                assert_eq!(transformation.0, board.t.clone());
                assert_eq!(transformation.1, salt.get_transformation_salt().clone());

                for i in 0..COMMITABLE_BOARD_TOTAL_CELLS {
                    let mut b = board.get_cell(i as usize).clone();
                    let mut s = salt.get_cell_salt(i as usize).clone();

                    b.ship_size = [None, None, None, None];
                    b.ship_id = None;
                    s = [None, None, None, None, None, s[CELL_SALT_SHIP_POSITION]];
                    assert_eq!((i as usize, b, s), cells[i as usize]);
                }
            }
            _ => {
                return assert!(false);
            }
        }
    }

    #[test]
    fn test_get_answer_ship_placement_small() {
        let mut rng = StdRng::seed_from_u64(42);
        let ship_id = 1;
        let ship_idx = 20;
        const SIDE: usize = COMMITABLE_BOARD_SIDE_LEN as usize;

        let mut board = empty_commitable_board();

        board.shuffled_cells[ship_idx].ship_size =
            [Some(true), Some(false), Some(false), Some(false)];
        board.shuffled_cells[ship_idx].ship_id = Some(ship_id);

        let positions = vec![
            ship_idx - 1 - SIDE,
            ship_idx - SIDE,
            ship_idx + 1 - SIDE,
            ship_idx - 1,
            ship_idx,
            ship_idx + 1,
            ship_idx - 1 + SIDE,
            ship_idx + SIDE,
            ship_idx + 1 + SIDE,
        ];

        let salt = Salter::new(&mut rng);

        let answer = get_answer_ship_placement(ship_id, &board, &salt);

        match answer {
            Answer::Cells(cells) => {
                assert_eq!(cells.len(), 9);
                assert!(positions.iter().all(|i| cells.contains(&(
                    *i,
                    board.get_cell(*i),
                    salt.get_cell_salt(*i)
                ))));
            }
            _ => {
                return assert!(false);
            }
        }
    }

    #[test]
    fn test_get_answer_ship_placement_big() {
        let mut rng = StdRng::seed_from_u64(42);
        let ship_id = 5;
        // statek na polach o indeksach 20,20+SIDE
        const SIDE: usize = COMMITABLE_BOARD_SIDE_LEN as usize;

        const SHIP_IDX: usize = 20;
        const SHIP_IDX2: usize = 20 + SIDE;

        let mut board = empty_commitable_board();

        board.shuffled_cells[SHIP_IDX].ship_size =
            [Some(true), Some(true), Some(false), Some(false)];
        board.shuffled_cells[SHIP_IDX].ship_id = Some(ship_id);

        board.shuffled_cells[SHIP_IDX2].ship_size =
            [Some(true), Some(true), Some(false), Some(false)];
        board.shuffled_cells[SHIP_IDX2].ship_id = Some(ship_id);

        let positions = vec![
            SHIP_IDX - 1 - SIDE,
            SHIP_IDX - SIDE,
            SHIP_IDX + 1 - SIDE,
            SHIP_IDX - 1,
            SHIP_IDX,
            SHIP_IDX + 1,
            SHIP_IDX - 1 + SIDE,
            SHIP_IDX + SIDE,
            SHIP_IDX + 1 + SIDE,
            SHIP_IDX - 1 + 2 * SIDE,
            SHIP_IDX + 2 * SIDE,
            SHIP_IDX + 1 + 2 * SIDE,
        ];

        let salt = Salter::new(&mut rng);

        let answer = get_answer_ship_placement(ship_id, &board, &salt);

        match answer {
            Answer::Cells(cells) => {
                assert_eq!(cells.len(), 12);
                assert!(positions.iter().all(|i| cells.contains(&(
                    *i,
                    board.get_cell(*i),
                    salt.get_cell_salt(*i)
                ))));
            }
            _ => {
                return assert!(false);
            }
        }
    }

    #[test]
    fn test_get_answer_all_placed() {
        let mut rng = StdRng::seed_from_u64(42);
        let mut board = empty_commitable_board();

        board.shuffled_cells[1].ship_size = [Some(true), Some(true), Some(false), Some(false)];
        board.shuffled_cells[1].ship_id = Some(5);

        let salt = Salter::new(&mut rng);

        let answer = get_answer_all_placed(&board, &salt);

        match answer {
            Answer::Cells(cells) => {
                for i in 0..COMMITABLE_BOARD_TOTAL_CELLS {
                    let mut b = board.get_cell(i as usize).clone();
                    let mut s = salt.get_cell_salt(i as usize).clone();
                    b.position = None;
                    s[CELL_SALT_SHIP_POSITION] = None;
                    assert_eq!((i as usize, b, s), cells[i as usize]);
                }
            }
            _ => {
                return assert!(false);
            }
        }
    }

    #[test]
    fn test_verify_hash_cell_ok1() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rng);

        let commitment = salt.create_commitment(&board);

        let mut cell = board.get_cell(0).clone();
        let mut s = salt.get_cell_salt(0).clone();

        cell.position = None;
        s[CELL_SALT_SHIP_POSITION] = None;

        assert!(verify_hash_cell(0, cell, s, &commitment));
    }

    #[test]
    fn test_verify_hash_cell_ok2() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rng);

        let commitment = salt.create_commitment(&board);

        let cell = board.get_cell(0).clone();
        let s = salt.get_cell_salt(0).clone();

        assert!(verify_hash_cell(0, cell, s, &commitment));
    }

    #[test]
    fn test_verify_hash_cell_wrong() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rng);

        let commitment = salt.create_commitment(&board);

        let mut cell = board.get_cell(0).clone();
        let mut s = salt.get_cell_salt(0).clone();

        cell.position = None;
        s[CELL_SALT_SHIP_POSITION] = None;
        cell.ship_id = Some(22);

        assert!(!verify_hash_cell(0, cell, s, &commitment));
    }
    #[test]

    fn test_verify_same_nones_ok() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rng);

        let mut cell = board.get_cell(0).clone();
        let mut s = salt.get_cell_salt(0).clone();

        cell.position = None;
        s[CELL_SALT_SHIP_POSITION] = None;

        assert!(verify_same_nones(cell, s));
    }
    #[test]

    fn test_verify_same_nones_wrong() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rng);

        let mut cell = board.get_cell(0).clone();
        let mut s = salt.get_cell_salt(0).clone();

        cell.position = None;
        s[CELL_SALT_SHIP_POSITION] = None;
        s[0] = None;

        assert!(!verify_same_nones(cell, s));
    }
    #[test]

    fn test_verify_legal_cells_ok() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rng);

        let v = vec![
            (0, board.get_cell(0), salt.get_cell_salt(0)),
            (1, board.get_cell(1), salt.get_cell_salt(1)),
        ];

        assert!(verify_legal_cells(&v));
    }
    #[test]

    fn test_verify_legal_cells_wrong() {
        let mut rng = StdRng::seed_from_u64(42);
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rng);

        let v = vec![
            (1, board.get_cell(0), salt.get_cell_salt(0)),
            (1, board.get_cell(1), salt.get_cell_salt(1)),
        ];

        assert!(!verify_legal_cells(&v));
    }

    #[test]

    fn test_verify_correct_ship_info_ok1() {
        let cell = Cell {
            ship_size: [Some(true), Some(true), Some(false), Some(false)],
            ship_id: Some(5),
            position: None,
        };
        assert!(verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_ok2() {
        let cell = Cell {
            ship_size: [Some(true), Some(false), Some(false), Some(false)],
            ship_id: Some(4),
            position: None,
        };
        assert!(verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_ok3() {
        let cell = Cell {
            ship_size: [Some(false), Some(false), Some(false), Some(false)],
            ship_id: Some(0),
            position: None,
        };
        assert!(verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_ok4() {
        let cell = Cell {
            ship_size: [Some(true), Some(true), Some(true), Some(true)],
            ship_id: Some(10),
            position: None,
        };
        assert!(verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_wrong1() {
        let cell = Cell {
            ship_size: [Some(true), Some(true), Some(false), Some(false)],
            ship_id: Some(9),
            position: None,
        };
        assert!(!verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_wrong2() {
        let cell = Cell {
            ship_size: [Some(false), Some(true), Some(true), Some(true)],
            ship_id: Some(8),
            position: None,
        };
        assert!(!verify_correct_ship_info(cell));
    }
    #[test]

    fn test_verify_correct_ship_info_wrong3() {
        let cell = Cell {
            ship_size: [Some(true), Some(false), Some(true), Some(false)],
            ship_id: Some(0),
            position: None,
        };
        assert!(!verify_correct_ship_info(cell));
    }
}
