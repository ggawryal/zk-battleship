use blake3::Hasher;
use crate::transformation::TransformationModulo;
use crate::board::Cell;
use crate::salt::{Salt, CellSalt};
use crate::config::COMMITABLE_BOARD_TOTAL_CELLS;
use serde_derive::{Serialize, Deserialize};

pub type Hash = [u8; 32];
type CellHash = [Option<Hash>; 6]; 


#[derive(Clone, Serialize, Deserialize)]
pub struct Commitment(
    pub Hash,
    #[serde(with = "serde_arrays")]
    pub [CellHash; COMMITABLE_BOARD_TOTAL_CELLS as usize]
);



pub fn hash_transformation<const MODULO:u32>(t: TransformationModulo<MODULO>, salt: Salt) -> Hash {
    hash(t.to_byte_array().iter().cloned(), salt)
}

pub fn hash_cell(cell: Cell, salt: CellSalt) -> CellHash  {
    cell.to_byte_arrays().iter()
        .zip(salt.iter())
        .map(|(x,s)| Some(hash((*x)?.iter().cloned(), (*s)?)))
        .collect::<Vec<Option<Hash>>>()
        .try_into()
        .unwrap()
}

pub fn hash_bool(b: bool, salt: Salt) -> Hash {
    hash([b as u8].iter().cloned(), salt)
}

fn hash(bytes: impl Iterator<Item=u8>, salt: Salt) -> Hash {
    let mut h = Hasher::new();
    h.update(&salt);
    h.update(&bytes.collect::<Vec<u8>>());
    return h.finalize().into();
}

#[cfg(test)]
mod tests {
    use super::*; 
    const MOD: u32 = 14;
    type Transformation = TransformationModulo<MOD>;

    #[test]
    fn test_hash() {
        let bytes = [1,2,3,4];
        let salt = [0; 32];
        let expected_result = [146, 28, 182, 65, 225, 143, 107, 25, 68, 178, 62, 200, 163, 75, 223, 50, 254, 121, 55, 186, 105, 101, 81, 115, 200, 221, 253, 120, 247, 143, 71, 172];
        assert_eq!(hash(bytes.iter().cloned(), salt), expected_result);
    }

    #[test]
    fn test_hash_for_different_bytes_is_different() {
        let bytes1 = [1,2,3,4];
        let bytes2 = [4,3,2,1];
        let salt = [0; 32];

        assert_ne!(hash(bytes1.iter().cloned(), salt), hash(bytes2.iter().cloned(), salt));
    }

    #[test]
    fn test_hash_for_different_salts_is_different() {
        let bytes = [1,2,3,4];
        let salt1 = [0; 32];
        let salt2 = [1; 32];

        assert_ne!(hash(bytes.iter().cloned(), salt1), hash(bytes.iter().cloned(), salt2));
    }
    
    #[test]
    fn test_hash_transformation() {
        let t = Transformation {
            v: (3,7),
            swaps_xy: true
        };
        let salt = [0; 32];
        
        for v_x in 0..MOD {
            for v_y in 0..MOD {
                for swaps_xy in [false, true] {
                    let s = Transformation {
                        v: (v_x, v_y),
                        swaps_xy: swaps_xy
                    };

                    let are_hashes_equal = hash_transformation(t, salt) == hash_transformation(s, salt);
                    let are_transformations_equal = t == s;
                    
                    assert_eq!(are_hashes_equal, are_transformations_equal);
                }
            }
        }
    }
    
}
