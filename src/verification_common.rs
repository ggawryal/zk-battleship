use crate::board::*;
use crate::config::*;
use crate::hash::{hash_cell, Commitment};
use crate::salt::*;
use crate::transformation::*;

pub type Transformation = TransformationModulo<TRANSFORMATION_MODULO>;

pub fn verify_hash_cell(idx: usize, cell: Cell, salt: CellSalt, commitment: &Commitment) -> bool {
    if idx >= COMMITABLE_BOARD_TOTAL_CELLS as usize {
        return false;
    }

    let correct_hash = commitment.1[idx];

    let partial_hash = hash_cell(cell, salt);

    return partial_hash
        .iter()
        .zip(correct_hash.iter())
        .all(|(x, s)| x.is_none() || x == s);
}

// sprawdza czy cell i salt maja w tych samych miejscach nony
pub fn verify_same_nones(cell: Cell, salt: CellSalt) -> bool {
    let bytes = cell.to_byte_arrays();

    return (0..bytes.len()).all(|i| bytes[i].is_none() == salt[i].is_none());
}

pub fn get_empty_cell() -> Cell {
    Cell {
        ship_size: [None; 4],
        ship_id: None,
        position: None,
    }
}
