use lib_zk_battleship::board::*;
use lib_zk_battleship::hash::*;
use lib_zk_battleship::rendering::*;
use lib_zk_battleship::salt::*;
use lib_zk_battleship::verification::*;
use lib_zk_battleship::verification_common::*;
use lib_zk_battleship::ingame_verification::*;
use lib_zk_battleship::connection::*;
use lib_zk_battleship::config::*;
use std::cmp::min;


use crossterm::{
    event::{self, Event as CEvent, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode},
};

use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

use rand::{rngs::SmallRng, Rng, SeedableRng};
use std::error::Error;
use std::io;
use std::sync::mpsc::channel;
use std::thread;
use std::time::Duration;
use tcp_channel::{ChannelRecv, ChannelSend};

fn exit_with_message<B : Backend>(terminal : &mut Terminal<B>, message : &str) {
        render_message(terminal, message);
        thread::sleep(Duration::from_millis(1000));
        disable_raw_mode().unwrap();
        terminal.show_cursor().unwrap();
        terminal.clear().unwrap();
}

fn verify_myself_single_query(board: &CommitableBoard, commitment: &Commitment, salter: &Salter, query: VerifyingQuery) -> bool {
    let answer = get_answer(board, salter, query);
    return verify_answer(query, answer, commitment);
}

fn verify_myself(board: &CommitableBoard, commitment: &Commitment, salter: &Salter) -> bool {
    for ship_id in 1..=10 {
        if !verify_myself_single_query(board, commitment, salter, VerifyingQuery::CorrectShipPlacement(ship_id)) {
            return false;
        }
    }
    return verify_myself_single_query(board, commitment, salter, VerifyingQuery::AllShipsPlaced())
        && verify_myself_single_query(board, commitment, salter, VerifyingQuery::CorrectBoard());
}


//Reuse code from verification phase
//Create dummy salter, commitment and then self verify 
fn strict_verify_board(board: &CommitableBoard) -> bool {
    let mut rng = SmallRng::from_rng(&mut rand::thread_rng()).unwrap();
    let mut salter = Salter::new(&mut rng);
    let commitment = salter.create_commitment(&board);

    return verify_myself(board, &commitment, &salter);
}

fn get_size_if_shot_at(mut board : Board, pos : (usize, usize)) -> u8 {
    board.board[pos.0][pos.1] = true;
    return min(4,get_all_cells_belonging_to_that_ship(&board, pos).len().try_into().unwrap());
}

fn main() -> Result<(), Box<dyn Error>> {
    /*
     *  TUI setup
     */

    enable_raw_mode().unwrap();

    let (tx, rx) = channel();
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || loop {
        if event::poll(tick_rate).unwrap() {
            if let CEvent::Key(key) = event::read().unwrap() {
                tx.send(Event::Input(key)).unwrap();
            }
        } 
    });

    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    /*
     *  Connecting with opponent
     */

    let host : bool;
    loop {
        render_message(&mut terminal, "If you want to host a game, press 1. Otherwise, press 2.");

        match rx.recv().unwrap() {
            Event::Input(event) => match event.code {
                KeyCode::Esc | KeyCode::Char('q') => {
                    exit_with_message(&mut terminal, "Quitting...");
                    return Ok(());
                }
                KeyCode::Char('1') => {
                    host = true;
                    break;
                }
                KeyCode::Char('2') => {
                    host = false;
                    break;
                }
                _ => {}
            },
        }
    }

    disable_raw_mode().unwrap();
    terminal.show_cursor().unwrap();
    let mut address = "0.0.0.0".to_string();
    if !host {
        address.clear();
        loop {
            render_message(&mut terminal, "Input address of the host:");

            match rx.recv().unwrap() {
                Event::Input(event) => match event.code {
                    KeyCode::Enter => {
                        break;
                    }   
                    KeyCode::Char(c) => {
                        address.push(c);
                    }
                    _ => {}
                },
            }
        }
    }
    terminal.hide_cursor().unwrap();
    enable_raw_mode().unwrap();
    terminal.clear()?;

    render_message(&mut terminal, "Connecting with your opponent.");

    let port = ":5314";
    address.push_str(port);
    let channel_opt = get_tcp_channel(host, &rx, address.as_str()); 

    if channel_opt.is_none() {
        exit_with_message(&mut terminal, "No connection established. Quitting...");
        return Ok(());
    }

    let (mut tcp_tx, mut tcp_rx) = channel_opt.unwrap();

    /*
     *  Phase 1: Getting player's input - ship placement
     */

    let mut ship_placement = ShipPlacementBoard::new();
    let mut correct_board = false;
    loop {
        render_ship_placement_phase(&mut terminal, &mut ship_placement);

        match rx.recv().unwrap() {
            Event::Input(event) => match event.code {
                KeyCode::Esc | KeyCode::Char('q') => {
                    break;
                }
                KeyCode::Enter => {
                    let mut res = CommitableBoard::from_board(ship_placement.get_board_ref(), &mut rand::thread_rng());

                    if STRICT_VERIFICATION_AT_PLACEMENT_PHASE && res.is_ok() && !strict_verify_board(&res.as_mut().unwrap()) {
                        res = Err(BoardParsingError);
                    }

                    match res {
                        Ok(_) => {
                            correct_board = true;
                            break;
                        }
                        _ => {
                            render_message(&mut terminal, "Incorrect ship placement!");
                            thread::sleep(Duration::from_millis(1200));
                        }
                    }
                }
                KeyCode::Char('x') => ship_placement.toggle(),
                KeyCode::Up => ship_placement.user_position.up(),
                KeyCode::Down => ship_placement.user_position.down(),
                KeyCode::Left => ship_placement.user_position.left(),
                KeyCode::Right => ship_placement.user_position.right(),
                _ => {}
            },
        }
    }

    if !correct_board {
        exit_with_message(&mut terminal, "Quitting...");
        return Ok(());
    }

    /*
     *  Phase 2: Creating commitments
     */

    let mut commitable_boards = Vec::<CommitableBoard>::new();
    let mut commitments = Vec::<Commitment>::new();
    let mut salts = Vec::<Salter>::new();

    for i in 0..ITERATIONS {
        if i * 100 % ITERATIONS == 0 {
            render_progress(
                &mut terminal,
                "Creating commitments",
                ((i * 100) / ITERATIONS) as u16,
            );
        }

        let mut rng = SmallRng::from_rng(&mut rand::thread_rng()).unwrap();
        let c_board = CommitableBoard::from_board(ship_placement.get_board_ref(), &mut rng).unwrap();
        let mut solarz = Salter::new(&mut rng);
    
        commitable_boards.push(c_board.clone());
        commitments.push(solarz.create_commitment(&c_board));
        salts.push(solarz);
    }

    /*
     *  Phase 3: Exchanging commitments
     */

    render_message(&mut terminal, "Exchanging commitments.");

    let oc_opt = tcp_exchange(&mut tcp_tx, &mut tcp_rx, host, Message::Commit(commitments.clone())); 
    let mut opponent_commitments = Vec::<Commitment>::new();
    match oc_opt {
        Some(Message::Commit(oc)) => opponent_commitments = oc,
        _ => (),
    }

    if opponent_commitments.len() != ITERATIONS { 
        exit_with_message(&mut terminal, "Exchange failed. Your opponent left or tried to cheat.");
        return Ok(());
    }

    /*
     *  Phase 4: Verification Phase
     */

    render_message(&mut terminal, "Preparing verification queries.");

    let mut verification_queries = Vec::<VerifyingQuery>::new();
    for _ in &opponent_commitments {
        let roll = rand::thread_rng().gen::<u64>() % 13;
        verification_queries.push(match roll {
            0..=9 => { VerifyingQuery::CorrectShipPlacement((roll+1).try_into().unwrap()) },
            10 => { VerifyingQuery::AllShipsPlaced() },
            11 => { VerifyingQuery::CorrectBoard() },
            _ => { VerifyingQuery::Play() },
        });
    }

    render_message(&mut terminal, "Exchanging verification queries.");
    
    let ovq_opt = tcp_exchange(&mut tcp_tx, &mut tcp_rx, host, Message::Verify(verification_queries.clone())); 
    let mut opponent_vq = Vec::<VerifyingQuery>::new();
    match ovq_opt {
        Some(Message::Verify(ovq)) => opponent_vq = ovq,
        _ => (),
    }

    if opponent_vq.len() != ITERATIONS { 
        exit_with_message(&mut terminal, "Exchange failed.");
        return Ok(());
    }

    render_message(&mut terminal, "Creating answers to your opponent's queries");
    
    let mut answers = Vec::<Answer>::new();
    let mut opponent_play_boards = Vec::<(CommitableBoard, Salter)>::new();
    let mut opponent_sink_helper = Vec::<(Commitment, Transformation)>::new();
    for i in 0..ITERATIONS as usize {
        let ans = get_answer(&commitable_boards[i], &salts[i], opponent_vq[i]);
        match opponent_vq[i] {
            VerifyingQuery::Play() => {
                match ans {
                    Answer::OK((trans, _)) => {
                        opponent_play_boards.push((commitable_boards[i].clone(), salts[i].clone()));
                        opponent_sink_helper.push((commitments[i].clone(), trans.clone()));
                    },
                    _ => (),
                }
            }
            _ => (),
        }
        answers.push(ans);
    }

    render_message(&mut terminal, "Exchanging answers.");
    
    let ans_opt = tcp_exchange(&mut tcp_tx, &mut tcp_rx, host, Message::Prove(answers.clone())); 
    let mut opponent_answers = Vec::<Answer>::new();
    match ans_opt {
        Some(Message::Prove(ans)) => opponent_answers = ans,
        _ => (),
    }

    if opponent_answers.len() != ITERATIONS { 
        exit_with_message(&mut terminal, "Exchange failed.");
        return Ok(());
    }

    render_message(&mut terminal, "Verifying.");
    
    let mut my_play_boards = Vec::<(Commitment, Transformation)>::new();
    for i in 0..ITERATIONS as usize {
        if !verify_answer(verification_queries[i], opponent_answers[i].clone(), &opponent_commitments[i]) {
            exit_with_message(&mut terminal, "Congratulations, your opponent tried to cheat.");
            return Ok(());
        }

        match verification_queries[i] {
            VerifyingQuery::Play() => { 
                match opponent_answers[i] {
                    Answer::OK((trans, _)) => my_play_boards.push((opponent_commitments[i].clone(), trans.clone())),
                    _ => (),
                }
            },
            _ => (),
        }
    }


    /*
     *  Phase 5: Toss a coin who will start
     */

    let my_toss = rand::thread_rng().gen::<bool>();
    let my_salt = random_salt(&mut rand::thread_rng());
    let hash = hash_bool(my_toss, my_salt);
    
    let other_hash = match tcp_exchange(&mut tcp_tx, &mut tcp_rx, host, Message::CoinToss(hash)).ok_or(NetworkError)? {
        Message::CoinToss(other_hash) => other_hash,
        _ => {exit_with_message(&mut terminal, "Coin toss failed. Your opponent left or tried to cheat."); return Ok(())}
    };
    let (other_toss, other_salt) = match tcp_exchange(&mut tcp_tx, &mut tcp_rx, host, Message::CoinTossDecommit(my_toss, my_salt)).ok_or(NetworkError)? {
        Message::CoinTossDecommit(other_toss, other_salt) => (other_toss, other_salt),
        _ => {exit_with_message(&mut terminal, "Coin toss failed. Your opponent left or tried to cheat."); return Ok(())}
    };
    if hash_bool(other_toss, other_salt) != other_hash {
        exit_with_message(&mut terminal, "Congratulations, your opponent tried to cheat in the coin toss phase.");
        return Ok(());
    }

    let mut turn = host ^ (my_toss == other_toss);

    /*
     *  Phase 6: The Game itself
     */

    let mut my_ships_left = 10;
    let mut opponent_ships_left = 10;

    // for rendering
    let mut opponent_board = RenderBoard::new();
    let mut my_board = RenderBoard::from_board(ship_placement.get_board_ref());

    // for calculating size of the ship that has been hit
    let mut my_hit_board = Board::new([[false; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize]);
    let mut opponent_hit_board = Board::new([[false; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize]);
    
    // for checking if opponent is making illegal moves (in terms of shot position, not cheating)
    let mut strike_mask = StrikeMask::new();
    loop {
        render_game_phase(&mut terminal, &mut opponent_board, &mut my_board, turn);

        // our turn to shoot
        if turn {
            match rx.recv().unwrap() {
                Event::Input(event) => match event.code {
                    KeyCode::Esc | KeyCode::Char('q') => {
                        break;
                    }
                    KeyCode::Char('x') => {
                        let pos = opponent_board.user_position.pos;
                        let shot : Shot = ((pos.0 + 1) as u32, (pos.1 + 1) as u32); 
                        
                        // shooting at position pos
                        if tcp_tx.send(&Message::Strike(shot)).is_err() {
                            exit_with_message(&mut terminal, "Your opponent left the game.");
                            return Ok(());
                        }
                        let recv_res = tcp_rx.recv();
                        match recv_res {
                            Ok(Message::StrikeAns(ans)) => {
                                let ship_size = get_size_if_shot_at(my_hit_board.clone(), opponent_board.user_position.pos); 

                                // checking if number of answers is correct (number of play boards)
                                if ans.len() != my_play_boards.len() { 
                                    exit_with_message(&mut terminal, "Congratulations, your opponent tried to cheat.");
                                    return Ok(());
                                }
                                
                                // verifying opponent answers
                                let verified_ans : Vec<ShotResult> = my_play_boards.clone().iter().zip(ans)
                                    .map(|((c, t), a)| verify_shot_answer(ship_size, shot, *t, a, &c)).collect();
                                let res = verified_ans.iter().reduce(|a, x| if a == x { a } else { &ShotResult::Cheater });

                                match res {
                                    None => {
                                        exit_with_message(&mut terminal, "Something went really wrong.");
                                        return Ok(());
                                    },
                                    Some(ShotResult::Cheater) => {
                                        exit_with_message(&mut terminal, "Congratulations, your opponent tried to cheat.");
                                        return Ok(());
                                    }
                                    Some(ShotResult::Hit) => {
                                        my_hit_board.board[pos.0][pos.1] = true;
                                        render_message(&mut terminal, "Hit.");
                                        thread::sleep(Duration::from_millis(1000));
                                    },
                                    Some(ShotResult::Sink) => { 
                                        my_hit_board.board[pos.0][pos.1] = true;
                                        opponent_ships_left -= 1;
                                        render_message(&mut terminal, "Hit and sunk.");
                                        thread::sleep(Duration::from_millis(1000));
                                    },
                                    Some(ShotResult::Miss) => {
                                        render_message(&mut terminal, "Missed.");
                                        thread::sleep(Duration::from_millis(1000));
                                        turn = false;
                                    }
                                }

                                opponent_board.set_state(shot_res_to_cell_state(res.unwrap()));
                            },
                            Ok(Message::Illegal) => {
                                render_message(&mut terminal, "Your opponent: this was an illegal move. Do something else.");
                                thread::sleep(Duration::from_millis(1000));
                            },
                            Err(_) => { 
                                exit_with_message(&mut terminal, "Your opponent left the game.");
                                return Ok(());
                            }
                            _ => (),
                        }
                    },
                    KeyCode::Up => opponent_board.user_position.up(),
                    KeyCode::Down => opponent_board.user_position.down(),
                    KeyCode::Left => opponent_board.user_position.left(),
                    KeyCode::Right => opponent_board.user_position.right(),
                    _ => {}
                },
            }
        }
        // our opponent's turn
        else {
            let recv_res = tcp_rx.recv();
            match recv_res {
                Ok(Message::Strike(shot)) => {
                    let pos : (usize, usize) = ((shot.0 - 1) as usize, (shot.1 - 1) as usize);
                    
                    // checking if the shot is at a valid position
                    if !strike_mask.is_legal(pos) { 
                        if tcp_tx.send(&Message::Illegal).is_err() {
                            exit_with_message(&mut terminal, "Your opponent left the game.");
                            return Ok(());
                        }
                    } else {
                        strike_mask.strike_at(pos);
                        let ship_size = get_size_if_shot_at(opponent_hit_board.clone(), pos);
                        
                        // generating answers
                        let ans : Vec<ShotAnswer> = opponent_play_boards.clone().iter()
                            .map(|(x, y)| get_shot_answer(ship_size, shot, &x, &y)).collect();

                         if tcp_tx.send(&Message::StrikeAns(ans.clone())).is_err() {
                            exit_with_message(&mut terminal, "Your opponent left the game.");
                            return Ok(());
                         }

                        // here we are finding out what the result od opponent's strike is 
                        let res_vec : Vec<ShotResult> = opponent_sink_helper.clone().iter().zip(ans)
                            .map(|((c, t), a)| verify_shot_answer(ship_size, shot, *t, a, &c))
                            .collect();
                        let res = res_vec.iter().reduce(|a, x| if a == x { a } else { &ShotResult::Cheater });
                        match res {
                            Some(ShotResult::Hit) => {
                                opponent_hit_board.board[pos.0][pos.1] = true;
                            },
                            Some(ShotResult::Sink) => {
                                opponent_hit_board.board[pos.0][pos.1] = true;
                                my_ships_left -= 1;
                            },
                            Some(ShotResult::Miss) => {
                                turn = true;
                            },
                            None | Some(ShotResult::Cheater) => {
                                exit_with_message(&mut terminal, "Something went really wrong.");
                                return Ok(());
                            },
                        }
                        my_board.board[pos.0][pos.1] = shot_res_to_cell_state(res.unwrap());
                    }
                },
                Err(_) => { 
                    exit_with_message(&mut terminal, "Your opponent left the game.");
                    return Ok(());
                }
                _ => (),
            }
        }

        if my_ships_left == 0 {
            exit_with_message(&mut terminal, "You lose");
            return Ok(());
        }

        if opponent_ships_left == 0 {
            exit_with_message(&mut terminal, "You win");
            return Ok(());
        }
    }

    disable_raw_mode().unwrap();
    terminal.show_cursor().unwrap();
    Ok(())
}
