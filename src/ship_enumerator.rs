use std::collections::BTreeMap;
use rand::RngCore;
use rand::seq::SliceRandom;
use crate::board::ShipID;

pub struct ShipEnumerator {
    ships_id: BTreeMap<usize, Vec<ShipID> >
}

impl ShipEnumerator {
    pub fn new(count_of_ships_with_given_size: BTreeMap<usize, usize>) -> Self {
        let mut ships_id = BTreeMap::new();

        let mut current_ship_id: ShipID = 1;
        for (&size, &count) in count_of_ships_with_given_size.iter() {
            let first_ship_id_in_next_group = current_ship_id+(count as ShipID);
            ships_id.insert(size, (current_ship_id..first_ship_id_in_next_group).rev().collect());
            current_ship_id = first_ship_id_in_next_group;
        }

        return ShipEnumerator {ships_id};
    }

    pub fn shuffle(self: &mut Self, rng: &mut dyn RngCore) {
        for (_, elems) in self.ships_id.iter_mut() {
            elems.shuffle(rng);
        }
    }

    pub fn next_ship_id(self: &mut Self, size: usize) -> Option<ShipID> {
        return self.ships_id.get_mut(&size)?.pop();
    }
}


#[cfg(test)]
mod tests {
    use super::*; 
    use rand::SeedableRng;
    use rand::rngs::StdRng;

    #[test]
    fn test_ship_enumerator() {
        let mut ship_enumerator = ShipEnumerator::new(BTreeMap::from([(1,2), (3,1)]));

        assert_eq!(ship_enumerator.next_ship_id(1), Some(1));
        assert_eq!(ship_enumerator.next_ship_id(3), Some(3));
        assert_eq!(ship_enumerator.next_ship_id(2), None);
        assert_eq!(ship_enumerator.next_ship_id(1), Some(2));
        assert_eq!(ship_enumerator.next_ship_id(1), None);
        assert_eq!(ship_enumerator.next_ship_id(3), None);
    }

    #[test]
    fn test_ship_enumerator_shuffle() {
        let mut rng = StdRng::seed_from_u64(42);

        let mut ship_enumerator          = ShipEnumerator::new(BTreeMap::from([(1,100), (2,100)]));
        let mut ship_enumerator_shuffled = ShipEnumerator::new(BTreeMap::from([(1,100), (2,100)]));

        ship_enumerator_shuffled.shuffle(&mut rng);

        for ship_size in [1,2] {
            let sorted_order= (0..100).map(|_| ship_enumerator.next_ship_id(ship_size)).collect::<Vec<Option<ShipID>>>();
            let mut shuffled_order = (0..100).map(|_| ship_enumerator_shuffled.next_ship_id(ship_size)).collect::<Vec<Option<ShipID>>>();

            assert_ne!(shuffled_order, sorted_order);

            shuffled_order.sort();
            assert_eq!(shuffled_order, sorted_order);

        }
    }
}