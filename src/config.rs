pub const PLAYABLE_BOARD_SIDE_LEN: u32 = 10;
pub const TRANSFORMATION_MODULO: u32 = 14;

pub const COMMITABLE_BOARD_SIDE_LEN: u32 = PLAYABLE_BOARD_SIDE_LEN + 2;
pub const COMMITABLE_BOARD_TOTAL_CELLS: u32 = COMMITABLE_BOARD_SIDE_LEN * COMMITABLE_BOARD_SIDE_LEN;

pub const CELL_SALT_SIZE: usize = 6;
pub const CELL_SALT_SHIP_SIZE: usize = 0;
pub const CELL_SALT_SHIP_ID: usize = 4;
pub const CELL_SALT_SHIP_POSITION: usize = 5;

pub const SHIP_SIZE: [usize; 11] = [0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4];

pub const ITERATIONS: usize = 1000;

//set to false if you want to try to cheat other side with incorrect board
pub const STRICT_VERIFICATION_AT_PLACEMENT_PHASE: bool = true;
