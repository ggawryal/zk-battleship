use crate::config::*;
use crate::ship_enumerator::ShipEnumerator;
use crate::transformation::Coordinate;
use crate::transformation::TransformationModulo;
use rand::seq::SliceRandom;
use rand::{Rng, RngCore};
use serde_derive::{Deserialize, Serialize};
use itertools::Itertools;
use std::collections::BTreeMap;

type Transformation = TransformationModulo<TRANSFORMATION_MODULO>;

#[derive(Debug, Clone, PartialEq)]
pub struct GenericBoard<const SIDE_LEN: usize> {
    pub board: [[bool; SIDE_LEN]; SIDE_LEN],
}

impl<const SIDE_LEN: usize> GenericBoard<SIDE_LEN> {
    pub fn new(board: [[bool; SIDE_LEN]; SIDE_LEN]) -> Self {
        return Self { board };
    }
    pub fn has_ship_at(self: &Self, position: (usize, usize)) -> bool {
        return self.board[position.0][position.1];
    }
}

pub type ShipID = u8;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Copy)]
pub struct Cell {
    pub ship_size: [Option<bool>; 4],
    pub ship_id: Option<ShipID>,
    pub position: Option<(Coordinate, Coordinate)>,
}

impl Cell {
    pub fn to_byte_arrays(self: &Self) -> [Option<[u8; 8]>; 6] {
        return [
            self.ship_size[0].map(|x| u64::to_le_bytes(x as u64)),
            self.ship_size[1].map(|x| u64::to_le_bytes(x as u64)),
            self.ship_size[2].map(|x| u64::to_le_bytes(x as u64)),
            self.ship_size[3].map(|x| u64::to_le_bytes(x as u64)),
            self.ship_id.map(|x| u64::to_le_bytes(x as u64)),
            self.position.map(|(i, j)| {
                [i.to_le_bytes(), j.to_le_bytes()]
                    .concat()
                    .try_into()
                    .unwrap()
            }),
        ];
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct CommitableBoard {
    pub t: Transformation,
    pub shuffled_cells: [Cell; COMMITABLE_BOARD_TOTAL_CELLS as usize],
}

fn is_in_range<T: Ord>(a: T, low: T, high: T) -> bool {
    return low <= a && a < high;
}

fn is_in_range_2_d<T: Ord>(a: (T, T), low: (T, T), high: (T, T)) -> bool {
    return is_in_range(a.0, low.0, high.0) && is_in_range(a.1, low.1, high.1);
}

fn commitable_board_coordinate_to_real(c: Coordinate) -> Option<usize> {
    if is_in_range(c, 1, 1 + PLAYABLE_BOARD_SIDE_LEN) {
        return Some((c - 1) as usize);
    }
    return None;
}

fn as_i32_tuple(x: (usize, usize)) -> (i32, i32) {
    return (x.0 as i32, x.1 as i32);
}

fn as_usize_tuple(x: (i32, i32)) -> (usize, usize) {
    return (x.0 as usize, x.1 as usize);
}

pub type Board = GenericBoard<{ PLAYABLE_BOARD_SIDE_LEN as usize }>;

pub fn get_all_cells_belonging_to_that_ship<const SIDE_LEN: usize>(
    board: &GenericBoard<SIDE_LEN>,
    any_cell_with_that_ship: (usize, usize),
) -> Vec<(usize, usize)> {
    let mut res: Vec<(usize, usize)> = vec![any_cell_with_that_ship];
    const DIRECTIONS: [(i32, i32); 4] = [
        (0, 1),
        (0, -1),
        (1, 0),
        (-1, 0),
    ];

    for direction in DIRECTIONS {
        let mut current_pos = as_i32_tuple(any_cell_with_that_ship);
        while is_in_range_2_d(current_pos, (0, 0), (SIDE_LEN as i32, SIDE_LEN as i32))
            && board.has_ship_at(as_usize_tuple(current_pos))
        {
            if as_usize_tuple(current_pos) != any_cell_with_that_ship {
                res.push(as_usize_tuple(current_pos));
            }
            current_pos.0 = current_pos.0 + direction.0;
            current_pos.1 = current_pos.1 + direction.1;
        }
    }
    return res;
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct ShipData {
    id: ShipID,
    size: usize,
}

fn get_board_with_ships_id<const SIDE_LEN: usize>(
    board: &GenericBoard<SIDE_LEN>,
    ship_enumerator: &mut ShipEnumerator,
) -> Result<[[Option<ShipData>; SIDE_LEN]; SIDE_LEN], BoardParsingError> {
    let mut ships_id: [[Option<ShipData>; SIDE_LEN]; SIDE_LEN] = [[None; SIDE_LEN]; SIDE_LEN];
    for i in 0..SIDE_LEN {
        for j in 0..SIDE_LEN {
            if board.has_ship_at((i, j)) && ships_id[i][j].is_none() {
                let ship_cells = get_all_cells_belonging_to_that_ship(board, (i, j));
                let ship_id = ship_enumerator
                    .next_ship_id(ship_cells.len())
                    .ok_or(BoardParsingError)?
                    .clone();
                for ship_cell in ship_cells.iter() {
                    ships_id[ship_cell.0][ship_cell.1] = Some(ShipData {
                        id: ship_id,
                        size: ship_cells.len(),
                    });
                }
            }
        }
    }
    return Ok(ships_id);
}

#[derive(Debug, Clone)]
pub struct BoardParsingError;

impl CommitableBoard {
    //this method does only necessary board validation to correctly construct board
    //more meticulous validation could be done after running this
    pub fn from_board(board: &Board, rng: &mut dyn RngCore) -> Result<Self, BoardParsingError> {
        let t = Transformation {
            v: (
                rng.gen_range(0..TRANSFORMATION_MODULO),
                rng.gen_range(0..TRANSFORMATION_MODULO),
            ),
            swaps_xy: rng.gen::<bool>(),
        };

        let mut ship_enumerator = ShipEnumerator::new(
            SHIP_SIZE.iter()
            .filter(|x| **x > 0)
            .group_by(|x| **x)
            .into_iter()
            .map(|(x, group)| (x, group.count()))
            .collect::<BTreeMap<usize, usize>>()
        );

        ship_enumerator.shuffle(rng);

        let board_with_ships_id = get_board_with_ships_id(board, &mut ship_enumerator)?;

        let mut cells: Vec<Cell> = Vec::new();
        for i in 0..COMMITABLE_BOARD_SIDE_LEN {
            for j in 0..COMMITABLE_BOARD_SIDE_LEN {
                let ci = commitable_board_coordinate_to_real(i);
                let cj = commitable_board_coordinate_to_real(j);
                let is_cell_empty = ci.is_none()
                    || cj.is_none()
                    || board_with_ships_id[ci.unwrap()][cj.unwrap()].is_none();

                if is_cell_empty {
                    cells.push(Cell {
                        ship_size: [Some(false); 4],
                        ship_id: Some(0),
                        position: Some(t.transform((i, j))),
                    });
                } else {
                    let ship_data = board_with_ships_id[ci.unwrap()][cj.unwrap()].unwrap();
                    cells.push(Cell {
                        ship_size: (1..5)
                            .map(|i| Some(ship_data.size >= i))
                            .collect::<Vec<Option<bool>>>()
                            .try_into()
                            .unwrap(),
                        ship_id: Some(ship_data.id),
                        position: Some(t.transform((i, j))),
                    });
                }
            }
        }

        cells.shuffle(rng);
        return Ok(CommitableBoard {
            t: t,
            shuffled_cells: cells.try_into().unwrap(),
        });
    }

    pub fn get_cell(self: &Self, idx: usize) -> Cell {
        return self.shuffled_cells[idx];
    }

    pub fn get_transformation(self: &Self) -> Transformation {
        return self.t;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::rngs::StdRng;
    use rand::SeedableRng;
    use itertools::Itertools;
    use std::collections::HashSet;

    #[test]
    fn test_to_byte_array_nones() {
        let cell = Cell {
            ship_size: [None, None, None, None],
            ship_id: Some(1),
            position: None,
        };
        let b = cell.to_byte_arrays();
        assert!((0..6).all(|i| (i != 4) == b[i].is_none()));
    }

    const X: bool = true;
    const O: bool = false;

    #[test]
    fn test_get_all_cells_belonging_to_that_ship() {
        let board = GenericBoard::new([
            [X,X,X,O,O],
            [O,O,O,O,X],
            [X,O,O,O,X],
            [O,O,O,O,X],
            [O,O,O,O,X]
        ]);
        assert!(get_all_cells_belonging_to_that_ship(&board, (0,1)).len() == 3);
        assert!(get_all_cells_belonging_to_that_ship(&board, (2,0)).len() == 1);
        assert!(get_all_cells_belonging_to_that_ship(&board, (3,4)).len() == 4);
    }

    #[test]
    fn test_get_board_with_ships_id() {
        let board = GenericBoard::new([
            [X,X,X,O,O],
            [O,O,O,O,X],
            [X,O,O,O,X],
            [O,O,O,O,X],
            [O,O,O,O,X]
        ]); 
        let mut se = ShipEnumerator::new(BTreeMap::from([(1,1), (3,1), (4,1)]));

        let ship_len_1_data = Some(ShipData {
            id: 1,
            size: 1
        });
        let ship_len_3_data = Some(ShipData {
            id: 2,
            size: 3
        });
        let ship_len_4_data = Some(ShipData {
            id: 3,
            size: 4
        });

        let expected = [
            [ship_len_3_data, ship_len_3_data, ship_len_3_data, None, None],
            [None           , None           , None           , None, ship_len_4_data],
            [ship_len_1_data, None           , None           , None, ship_len_4_data],
            [None           , None           , None           , None, ship_len_4_data],
            [None           , None           , None           , None, ship_len_4_data]
        ];
        assert_eq!(get_board_with_ships_id(&board, &mut se).unwrap(), expected);
    }


    #[test]
    fn test_from_board_fails_when_too_long_ship() {
        let board = Board::new([
            [X, O, O, O, O, O, O, O, X, O],
            [O, O, O, O, O, O, X, O, O, O],
            [O, O, O, X, X, O, O, O, O, O],
            [O, O, O, O, O, O, O, O, O, O],
            [O, X, O, X, O, O, O, O, O, O],
            [O, O, O, X, O, X, O, O, O, O],
            [O, O, O, X, O, X, O, O, O, O],
            [O, O, O, O, O, X, O, O, X, X],
            [O, X, X, X, O, X, O, O, O, O],
            [O, O, O, O, O, X, O, O, X, X],
        ]);
        assert!(CommitableBoard::from_board(&board, &mut rand::thread_rng()).is_err());
    }

    #[test]
    fn test_from_board_fails_when_wrong_ship_types() {
        //three 3-ships instead of two
        let board = Board::new([
            [X, X, X, X, O, O, O, O, X, O],
            [O, O, O, O, O, O, X, O, O, O],
            [O, O, O, X, X, O, O, O, O, O],
            [O, O, O, O, O, O, O, O, O, O],
            [O, X, O, X, O, O, O, O, O, O],
            [O, O, O, X, O, O, O, O, O, O],
            [O, O, O, X, O, O, O, O, O, O],
            [O, O, O, O, O, O, O, X, X, X],
            [O, X, X, X, O, O, O, O, O, O],
            [O, O, O, O, O, X, O, O, X, X],
        ]);
        assert!(CommitableBoard::from_board(&board, &mut rand::thread_rng()).is_err());
    }

    fn example_ok_board() -> Board {
        return Board::new([
            [X, X, X, X, O, O, O, O, X, O],
            [O, O, O, O, O, O, X, O, O, O],
            [O, O, O, X, X, O, O, O, O, O],
            [O, O, O, O, O, O, O, O, O, O],
            [O, X, O, X, O, O, O, O, O, O],
            [O, O, O, X, O, O, O, O, O, O],
            [O, O, O, X, O, O, O, O, O, O],
            [O, O, O, O, O, O, O, O, X, X],
            [O, X, X, X, O, O, O, O, O, O],
            [O, O, O, O, O, X, O, O, X, X],
        ]);
    }

    #[test]
    fn test_from_board_works_for_correct_board() {
        assert!(CommitableBoard::from_board(&example_ok_board(), &mut rand::thread_rng()).is_ok());
    }

    #[test]
    fn test_from_board_sets_all_data_to_some() {
        let mut rng = StdRng::seed_from_u64(42);
        let commitable_board = CommitableBoard::from_board(&example_ok_board(), &mut rng).unwrap();
        assert!(commitable_board.shuffled_cells.iter().map(Cell::to_byte_arrays).all(|x| x.iter().all(Option::is_some)));
    }

    #[test]
    fn test_from_board_gives_different_transformations() {
        //after changing implementation, this test may fail with probability 1/392 -> change seed if not works
        let mut rng = StdRng::seed_from_u64(42);
        let commitable_board1 = CommitableBoard::from_board(&example_ok_board(), &mut rng);
        let commitable_board2 = CommitableBoard::from_board(&example_ok_board(), &mut rng);
        assert_ne!(commitable_board1.unwrap(), commitable_board2.unwrap());
    }

    #[test]
    fn test_from_board_shuffles_cells() {
        let mut rng = StdRng::seed_from_u64(42);
        let commitable_board1 = CommitableBoard::from_board(&example_ok_board(), &mut rng).unwrap();
        let commitable_board2 = CommitableBoard::from_board(&example_ok_board(), &mut rng).unwrap();
        assert_ne!(
            commitable_board1.shuffled_cells.iter().map(|c| c.ship_size).collect::<Vec<[Option<bool>; 4]>>(), 
            commitable_board2.shuffled_cells.iter().map(|c| c.ship_size).collect::<Vec<[Option<bool>; 4]>>()
        );
    }
    #[test]
    fn test_from_board_gives_correct_cell_positions_after_transformation() {
        let mut rng = StdRng::seed_from_u64(42);
        let commitable_board = CommitableBoard::from_board(&example_ok_board(), &mut rng).unwrap();
        let positions: HashSet<(Coordinate, Coordinate)> = commitable_board.shuffled_cells.iter().map(|c| c.position.unwrap()).collect();
        
        let expected_positions: HashSet<(Coordinate, Coordinate)> = (0..COMMITABLE_BOARD_SIDE_LEN)
            .cartesian_product(0..COMMITABLE_BOARD_SIDE_LEN)
            .map(|(i,j)| commitable_board.t.transform((i,j)))
            .collect();

        assert_eq!(positions, expected_positions);

    }
}
