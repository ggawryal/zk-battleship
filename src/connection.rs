use crate::hash::*;
use crate::rendering::*;
use crate::verification::*;
use crate::salt::*;
use crate::board::*;
use crate::ingame_verification::*;

use crossterm::{
    event::{KeyCode, KeyEvent},
};

use serde_derive::{Deserialize, Serialize};
use std::net::{TcpListener, TcpStream};
use tcp_channel::{ChannelRecv, ChannelSend, ReceiverBuilder, SenderBuilder, Sender, Receiver, BigEndian};
use std::fmt;
use std::time::*;
use std::thread::sleep;


#[derive(Debug, Clone)]
pub struct NetworkError;
impl fmt::Display for NetworkError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Network Error")
    }
}
impl std::error::Error for NetworkError {}

#[derive(Serialize, Deserialize)]
pub enum Message {
    Commit(Vec<Commitment>),
    Verify(Vec<VerifyingQuery>),
    Prove(Vec<Answer>),
    CoinToss(Hash),
    CoinTossDecommit(bool, Salt),
    Strike(Shot),
    StrikeAns(Vec<(usize, Cell, CellSalt)>),
    Illegal,
}

pub fn init_connection(host: bool, 
                       rx: &std::sync::mpsc::Receiver<Event<KeyEvent>>, 
                       address : &str
                      ) -> Option<TcpStream> {
    if host {
        let bind_res = TcpListener::bind(address);
        if bind_res.is_err() {
            return None;
        }
        let listener = bind_res.unwrap();
        listener.set_nonblocking(true).ok()?;
        loop {
            match listener.accept() {
                Ok((tc, _)) => return Some(tc),
                Err(_) => (),
            }

            match rx.try_recv() {
                Ok(Event::Input(event)) => match event.code {
                    KeyCode::Esc | KeyCode::Char('q') => {
                        break;
                    },
                    _ => {}
                },
                Err(_) => (),
            }
    
            // We actively wait because we want user to be able to quit
            // so we cannot block on connection
            sleep(Duration::from_millis(200));
        }
    } else {
        loop {
            match TcpStream::connect(address) {
                Ok(tc) => return Some(tc),
                Err(_) => (),
            }

            match rx.try_recv() {
                Ok(Event::Input(event)) => match event.code {
                    KeyCode::Esc | KeyCode::Char('q') => {
                        break;
                    }
                    _ => {},
                },
                Err(_) => (),
            }

            // We actively wait because we want user to be able to quit
            // so we cannot block on connection
            sleep(Duration::from_millis(200));
        }
    }

    None
}

pub fn get_tcp_channel(
    host : bool, 
    rx : &std::sync::mpsc::Receiver<Event<KeyEvent>>,
    address : &str
    ) -> Option<(Sender<Message, BigEndian, TcpStream>, Receiver<Message, BigEndian, TcpStream>)> {
    let tcp_stream_opt = init_connection(host, rx, address);

    if tcp_stream_opt.is_none() {
        return None;
    }

    let tcp_stream = tcp_stream_opt.unwrap();
    let tcp_rx = ReceiverBuilder::realtime()
        .with_type::<Message>()
        .build(tcp_stream.try_clone().unwrap());
    let tcp_tx = SenderBuilder::realtime()
        .with_type::<Message>()
        .build(tcp_stream.try_clone().unwrap());
    Some((tcp_tx, tcp_rx))
}

pub fn tcp_exchange(
    tcp_tx : &mut Sender<Message, BigEndian, TcpStream>, 
    tcp_rx : &mut Receiver<Message, BigEndian, TcpStream>, 
    host : bool, 
    to_send : Message
    ) -> Option<Message> {
    let mut ret_opt = None;
    if host {
        if tcp_tx.send(&to_send).is_err() {
            return None;
        }

        let recv_res = tcp_rx.recv();
        match recv_res {
            Ok(m) => ret_opt = Some(m),
            _ => (),
        }
    } else {
        let recv_res = tcp_rx.recv();
        match recv_res {
            Ok(m) => ret_opt = Some(m),
            _ => (),
        }

        if tcp_tx.send(&to_send).is_err() {
            return None;
        }
    }

    ret_opt
}
