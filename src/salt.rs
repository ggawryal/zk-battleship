use crate::board::CommitableBoard;
use crate::config::COMMITABLE_BOARD_TOTAL_CELLS;
use crate::hash::{hash_cell, hash_transformation, Commitment};
use rand::{Rng, RngCore};

pub type Salt = [u8; 32];
pub type CellSalt = [Option<Salt>; 6];

pub fn random_salt(rng: &mut dyn RngCore) -> Salt{
    rng.gen()
}

#[derive(Clone)]
pub struct Salter {
    transformation_salt: Salt,
    cell_salts: [CellSalt; COMMITABLE_BOARD_TOTAL_CELLS as usize],
}

impl Salter {
    pub fn new(rng: &mut dyn RngCore) -> Self {
        fn generate_cell_salt(rng: &mut dyn RngCore) -> CellSalt {
            return (0..6)
                .map(|_| Some(random_salt(rng)))
                .collect::<Vec<Option<Salt>>>()
                .try_into()
                .unwrap();
        }

        return Salter {
            transformation_salt: random_salt(rng),
            cell_salts: (0..COMMITABLE_BOARD_TOTAL_CELLS)
                .map(|_| generate_cell_salt(rng))
                .collect::<Vec<CellSalt>>()
                .try_into()
                .unwrap(),
        };
    }

    pub fn create_commitment(self: &mut Self, commitable_board: &CommitableBoard) -> Commitment {
        let transformation_hash = hash_transformation(commitable_board.t, self.transformation_salt);
        let cell_hashes = commitable_board
            .shuffled_cells
            .iter()
            .cloned()
            .zip(self.cell_salts.iter().cloned())
            .map(|(cell, salt)| hash_cell(cell, salt))
            .collect::<Vec<CellSalt>>()
            .try_into()
            .unwrap();

        return Commitment(transformation_hash, cell_hashes);
    }

    pub fn get_cell_salt(self: &Self, idx: usize) -> CellSalt {
        return self.cell_salts[idx];
    }

    pub fn get_transformation_salt(self: &Self) -> Salt {
        return self.transformation_salt;
    }
}
