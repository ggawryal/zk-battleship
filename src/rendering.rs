use crate::board::*;
use crate::ingame_verification::*;
use crate::config::*;

use tui::{
    backend::Backend,
    widgets::{Table, Block, Row, Borders, Paragraph, Gauge},
    layout::{Constraint, Layout, Direction, Alignment, Rect},
    text::{Spans, Span},
    Terminal,
};


pub const RENDERED_BOARD_H : u16 = 12;
pub const RENDERED_BOARD_W : u16 = 21;

pub enum Event<I> {
    Input(I),
}

pub struct UserPosition {
    pub pos : (usize, usize),
}

impl UserPosition {
    pub fn up(self : &mut Self) {
        if self.pos.0 > 0 {
            self.pos.0 -= 1;
        }
    }

    pub fn down(self : &mut Self) {
        if self.pos.0 < PLAYABLE_BOARD_SIDE_LEN as usize - 1 {
            self.pos.0 += 1;
        }
    }

    pub fn left(self : &mut Self) {
        if self.pos.1 > 0 {
            self.pos.1 -= 1;
        }
    }

    pub fn right(self : &mut Self) {
        if self.pos.1 < PLAYABLE_BOARD_SIDE_LEN as usize - 1 {
            self.pos.1 += 1;
        }
    }
}

pub struct ShipPlacementBoard {
    pub user_position : UserPosition,
    data : Board,
}

impl ShipPlacementBoard {
    pub fn new() -> Self {
        ShipPlacementBoard {
            user_position : UserPosition{pos : (0, 0)},
            data : Board::new([[false; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize]),
        }
    }

    pub fn get_board_ref(&self) -> &Board {
        &self.data
    }

    pub fn toggle(self : &mut Self) {
        let (x, y) = self.user_position.pos;
        self.data.board[x][y] = !self.data.board[x][y];
    }

    pub fn to_tui_table(&self) -> Table<'static> {
        let mut tui_rows = Vec::<Row>::new();
        for (i, r) in self.data.board.iter().enumerate() {
            let mut raw_row = r.map(|b| match b { true => "#", _ => "."});
            if self.user_position.pos.0 == i {
                raw_row[self.user_position.pos.1] = "O";
            }
            tui_rows.push(Row::new(raw_row));
        }

        return Table::new(tui_rows)
            .widths(&[Constraint::Length(1); PLAYABLE_BOARD_SIDE_LEN as usize])
            .block(Block::default().borders(Borders::ALL).title("Battlefield"));
    }
}

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum CellRenderState {
    Hidden,
    Hit,
    Miss,
    Ship,
}

pub struct RenderBoard {
    pub user_position : UserPosition,
    pub board : [[CellRenderState; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize],
}

impl RenderBoard {
    pub fn new() -> Self {
        RenderBoard {
            user_position : UserPosition{pos : (0, 0)},
            board : [[CellRenderState::Hidden; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize],
        }
    }

    pub fn from_board(board : &Board) -> Self {
        let mut b = [[CellRenderState::Hidden; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize];
        for i in 0..PLAYABLE_BOARD_SIDE_LEN as usize {
            for j in 0..PLAYABLE_BOARD_SIDE_LEN as usize {
                b[i][j] = if board.board[i][j] {CellRenderState::Ship} else {CellRenderState::Miss};
            }
        }

        RenderBoard {
            user_position : UserPosition{pos : (0, 0)},
            board : b,
        }
    }

    pub fn set_state(self : &mut Self, state : CellRenderState) {
        let (x, y) = self.user_position.pos;
        self.board[x][y] = state;
    }

    pub fn to_tui_table(&self, turn : bool) -> Table<'static> {
        let mut tui_rows = Vec::<Row>::new();
        for (i, r) in self.board.iter().enumerate() {
            let mut raw_row = r.map(|b| match b { 
                CellRenderState::Hidden => "*",
                CellRenderState::Hit => "x",
                CellRenderState::Ship => "#",
                _ => "."
            });
            if self.user_position.pos.0 == i && turn {
                raw_row[self.user_position.pos.1] = "O";
            }
            tui_rows.push(Row::new(raw_row));
        }

        return Table::new(tui_rows)
            .widths(&[Constraint::Length(1); PLAYABLE_BOARD_SIDE_LEN as usize]);
    }
}

fn get_sp_info() -> Paragraph<'static> { 
    Paragraph::new(vec![Spans::from(vec![Span::raw(" ")]),
                        Spans::from(vec![Span::raw(" ")]),
                        Spans::from(vec![Span::raw("Controls:")]),
                        Spans::from(vec![Span::raw("Arrows - change selected cell")]),
                        Spans::from(vec![Span::raw("x - toggle cell")]),
                        Spans::from(vec![Span::raw("Enter - submit ship placement")]),
                        Spans::from(vec![Span::raw("Esc or q - quit")]),
                        Spans::from(vec![Span::raw(" ")]),
                        Spans::from(vec![Span::raw("Available ships:")]),
                        Spans::from(vec![Span::raw("4 x #")]),
                        Spans::from(vec![Span::raw("3 x ##")]),
                        Spans::from(vec![Span::raw("2 x ###")]),
                        Spans::from(vec![Span::raw("1 x ####")]),
    ])
        .block(Block::default().borders(Borders::ALL))
        .alignment(Alignment::Center)
}

pub fn center_board(bounds : Rect) -> Rect {
        let margin_h : u16;
        let margin_w : u16;

        if bounds.height > RENDERED_BOARD_H && bounds.width > RENDERED_BOARD_W {
            margin_h = (bounds.height - RENDERED_BOARD_H)/2;
            margin_w = (bounds.width - RENDERED_BOARD_W)/2;
        }

        else {
            margin_h = 0;
            margin_w = 0;
        }

        let c1 = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(margin_h), Constraint::Length(RENDERED_BOARD_H), Constraint::Length(margin_h)])
            .split(bounds);
        let c2 = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Length(margin_w), Constraint::Length(RENDERED_BOARD_W), Constraint::Length(margin_w)])
            .split(c1[1]);
        c2[1]
}

pub fn shot_res_to_cell_state(res : &ShotResult) -> CellRenderState {
    match res {
        ShotResult::Hit | ShotResult::Sink => CellRenderState::Hit,
        ShotResult::Miss => CellRenderState::Miss,
        _ => CellRenderState::Hidden,
    }
}

pub fn render_ship_placement_phase<B : Backend>(t : &mut Terminal<B>, s : &mut ShipPlacementBoard) {
    t.draw(|f| {
        let rect = f.size();
        let v_chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(3), Constraint::Percentage(90)])
            .split(rect);
        let h_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
            .split(v_chunks[1]);

        f.render_widget(get_message("Place your ships!"), v_chunks[0]);
        f.render_widget(get_sp_info(), h_chunks[0]);
        f.render_widget(s.to_tui_table(), center_board(h_chunks[1]));
    }).unwrap();
}

pub fn render_game_phase<B : Backend>(t : &mut Terminal<B>, s : &mut RenderBoard, p : &mut RenderBoard, turn : bool) {
    t.draw(|f| {
        let rect = f.size();
        let v_chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(3), Constraint::Percentage(90)])
            .split(rect);
        let h_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
            .split(v_chunks[1]);

        let message = match turn { true => "Your turn!", _ => "Your Opponent's turn"};
        f.render_widget(get_message(message), v_chunks[0]);
        f.render_widget(s.to_tui_table(turn)
                        .block(Block::default().borders(Borders::ALL).title("Your opponent")),
                        center_board(h_chunks[0]));
        f.render_widget(p.to_tui_table(false) 
                        .block(Block::default().borders(Borders::ALL).title("You")),
                        center_board(h_chunks[1]));
    }).unwrap();
}

fn get_message(message : &str) -> Paragraph<'_> {
   Paragraph::new(vec![Spans::from(vec![Span::raw(message)])])
        .block(Block::default().borders(Borders::ALL))
        .alignment(Alignment::Center) 
}

pub fn render_message<B : Backend>(t : &mut Terminal<B>, message : &str) {
    t.draw(|f| {
        let rect = f.size();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Percentage(40), Constraint::Length(3), Constraint::Percentage(40)])
            .split(rect);
        f.render_widget(get_message(message), chunks[1]);
    }).unwrap();
}

fn get_progress(percent : u16) -> Gauge<'static> {
    Gauge::default()
    .percent(percent)
}

pub fn render_progress<B : Backend>(t : &mut Terminal<B>, message : &str, percent : u16) {
    t.draw(|f| {
        let rect = f.size();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Percentage(40), Constraint::Length(3), Constraint::Length(3), Constraint::Percentage(40)])
            .split(rect);
        f.render_widget(get_message(message), chunks[1]);
        f.render_widget(get_progress(percent), chunks[2]);
    }).unwrap();
}
