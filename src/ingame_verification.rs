use crate::board::*;
use crate::config::*;
use crate::hash::Commitment;
use crate::salt::*;
use crate::transformation::*;
use crate::verification_common::*;

use std::cmp::min;

pub type Shot = (Coordinate, Coordinate);
pub type ShotAnswer = (usize, Cell, CellSalt);

#[derive(Debug, Clone, PartialEq)]
pub enum ShotResult {
    Miss,
    Hit,
    Sink,
    Cheater,
}

pub struct StrikeMask {
    mask : [[bool; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize],
}

impl StrikeMask {
    pub fn new() -> Self {
        StrikeMask { mask : [[false; PLAYABLE_BOARD_SIDE_LEN as usize]; PLAYABLE_BOARD_SIDE_LEN as usize]}
    }

    pub fn is_legal(&self, pos : (usize, usize)) -> bool {
        if pos.0 >= PLAYABLE_BOARD_SIDE_LEN as usize || pos.1 >= PLAYABLE_BOARD_SIDE_LEN as usize {
            return false;
        }
        !self.mask[pos.0][pos.1]
    }

    pub fn strike_at(&mut self, pos : (usize, usize)) -> () {
        self.mask[pos.0][pos.1] = true;
    }
}

fn hide_fields(ship_size: u8, idx: usize, board: &CommitableBoard, salter: &Salter) -> ShotAnswer {
    let mut cell = get_empty_cell();
    let mut cell_salt: CellSalt = [None; 6];

    cell.position = board.get_cell(idx).position;
    cell_salt[CELL_SALT_SHIP_POSITION] = salter.get_cell_salt(idx)[CELL_SALT_SHIP_POSITION];

    for i in 0..(min(ship_size + 1, 4)) {
        cell.ship_size[i as usize] = board.get_cell(idx).ship_size[i as usize];
        cell_salt[i as usize] = salter.get_cell_salt(idx)[i as usize];
    }

    return (idx, cell, cell_salt);
}

// ship_size - "do co najmniej jakiego rozmiaru statku nalezy pole, w ktore strzelamy, jezeli jest statkiem". UWAGA NIE WIEKSZE NIZ 4
// W przypadku kiedy wiadomo, że pole jest puste, można ustawić cokolwiek, byle było to stale pomiedzy graczami (w szczególności deterministyczne)
// shot - koordynaty pola z zakresu [1,10] x [1,10].
pub fn get_shot_answer(
    ship_size: u8,
    shot: Shot,
    board: &CommitableBoard,
    salter: &Salter,
) -> ShotAnswer {
    assert!(ship_size <= 4);

    let position = board.get_transformation().transform(shot);

    let mut target_cell = (0..COMMITABLE_BOARD_TOTAL_CELLS)
        .filter(|i| board.get_cell(*i as usize).position.unwrap() == position);

    let idx: usize = target_cell.next().unwrap() as usize;
    //upewniam się, że tylko jeden pasuje
    assert!(target_cell.next() == None);

    let result = hide_fields(ship_size, idx, board, salter);
    return result;
}

pub fn verify_shot_answer(
    ship_size: u8,
    shot: Shot,
    transformation: Transformation,
    answer: ShotAnswer,
    commitment: &Commitment,
) -> ShotResult {
    assert!(ship_size <= 4);

    let position = transformation.transform(shot);

    // jeżeli wysłał złą pozycję
    if (answer.1.position.is_none() || answer.1.position.unwrap() != position) ||
    !verify_same_nones(answer.1, answer.2) //jezeli nie zgadzaja sie nony
        || !verify_hash_cell(answer.0, answer.1, answer.2, commitment)
    // jezeli nie zgadza sie hash
    {
        print!("1!");
        return ShotResult::Cheater;
    }

    // jeżeli wysłał nam za mało informacji to oszukuje
    if !(0..(min(ship_size + 1, 4))).all(|i| !answer.1.ship_size[i as usize].is_none()) {
        return ShotResult::Cheater;
    }

    if answer.1.ship_size[0].unwrap() == false {
        return ShotResult::Miss;
    } else {
        // jezeli właśnie dowiedziałem się, że statek w który trafiam ma rozmiar dokladnie ship_size
        if ship_size == 4 || !answer.1.ship_size[ship_size as usize].unwrap() {
            return ShotResult::Sink;
        } else {
            return ShotResult::Hit;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type Transformation14 = TransformationModulo<14>;

    fn empty_commitable_board() -> CommitableBoard {
        let mut shuffled_cells: [Cell; COMMITABLE_BOARD_TOTAL_CELLS as usize] =
            [get_empty_cell(); COMMITABLE_BOARD_TOTAL_CELLS as usize];

        for i in 0..COMMITABLE_BOARD_TOTAL_CELLS {
            let x = i % COMMITABLE_BOARD_SIDE_LEN;
            let y = i / COMMITABLE_BOARD_SIDE_LEN;
            shuffled_cells[i as usize] = Cell {
                ship_size: [Some(false), Some(false), Some(false), Some(false)],
                ship_id: Some(0),
                position: Some((x + 1, y + 1)),
            };
        }

        return CommitableBoard {
            t: Transformation14 {
                v: (1, 1),
                swaps_xy: false,
            },
            shuffled_cells: shuffled_cells,
        };
    }

    #[test]
    fn test_get_shot_answer_empty() {
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rand::thread_rng());

        let ship_size = 0;
        let shot = (1, 1);

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            answer.1.position.unwrap(),
            board.get_transformation().transform(shot)
        );

        assert!(!answer.1.ship_size[0].is_none());
        assert!(answer.1.ship_size[1].is_none());
    }

    #[test]
    fn test_get_shot_answer_size1() {
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rand::thread_rng());

        let ship_size = 1;
        let shot = (2, 2);

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            answer.1.position.unwrap(),
            board.get_transformation().transform(shot)
        );

        assert!(!answer.1.ship_size[0].is_none());
        assert!(!answer.1.ship_size[1].is_none());
        assert!(answer.1.ship_size[2].is_none());
    }

    #[test]
    fn test_get_shot_answer_size4() {
        let board = empty_commitable_board();
        let salt = Salter::new(&mut rand::thread_rng());

        let ship_size = 4;
        let shot = (3, 5);

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            answer.1.position.unwrap(),
            board.get_transformation().transform(shot)
        );

        assert!(!answer.1.ship_size[0].is_none());
        assert!(!answer.1.ship_size[1].is_none());
        assert!(!answer.1.ship_size[2].is_none());
        assert!(!answer.1.ship_size[3].is_none());
    }

    #[test]
    fn test_verify_shot_answer_sink_size4() {
        let mut board = empty_commitable_board();
        board.shuffled_cells[20].ship_size = [Some(true); 4];
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 4;
        let shot = board
            .get_transformation()
            .inverse_transform(board.shuffled_cells[20].position.unwrap());

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Sink
        );
    }

    #[test]
    fn test_verify_shot_wrong_position() {
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rand::thread_rng());

        let shot = (2, 2);
        let commitment = salt.create_commitment(&board);

        assert_eq!(
            verify_shot_answer(
                1,
                shot,
                board.get_transformation(),
                (0, board.get_cell(0), salt.get_cell_salt(0)),
                &commitment
            ),
            ShotResult::Cheater
        );
    }

    #[test]
    fn test_verify_shot_wrong_idx() {
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rand::thread_rng());
        let commitment = salt.create_commitment(&board);

        let idx = 20;
        let shot = board
            .get_transformation()
            .inverse_transform(board.shuffled_cells[idx].position.unwrap());

        assert_eq!(
            verify_shot_answer(
                1,
                shot,
                board.get_transformation(),
                (idx - 1, board.get_cell(idx), salt.get_cell_salt(idx)),
                &commitment
            ),
            ShotResult::Cheater
        );
    }

    #[test]
    fn test_verify_shot_answer_wrong_ship_size_len() {
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 4;
        let shot = (8, 10);

        let mut answer = get_shot_answer(ship_size, shot, &board, &salt);
        answer.1.ship_size[0] = None;

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Cheater
        );
    }

    #[test]
    fn test_verify_shot_answer_sink_size1() {
        let mut board = empty_commitable_board();
        board.shuffled_cells[20].ship_size = [Some(true), Some(false), Some(false), Some(false)];
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 1;
        let shot = board
            .get_transformation()
            .inverse_transform(board.shuffled_cells[20].position.unwrap());

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Sink
        );
    }

    #[test]
    fn test_verify_shot_answer_hit_size1() {
        let mut board = empty_commitable_board();
        board.shuffled_cells[20].ship_size = [Some(true), Some(true), Some(false), Some(false)];
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 1;
        let shot = board
            .get_transformation()
            .inverse_transform(board.shuffled_cells[20].position.unwrap());

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Hit
        );
    }

    #[test]
    fn test_verify_shot_answer_hit_size3() {
        let mut board = empty_commitable_board();
        board.shuffled_cells[20].ship_size = [Some(true), Some(true), Some(true), Some(true)];
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 3;
        let shot = board
            .get_transformation()
            .inverse_transform(board.shuffled_cells[20].position.unwrap());

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Hit
        );
    }

    #[test]
    fn test_verify_shot_answer_miss_size0() {
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 0;
        let shot = (10, 9);

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Miss
        );
    }

    #[test]
    fn test_verify_shot_answer_miss_size4() {
        let board = empty_commitable_board();
        let mut salt = Salter::new(&mut rand::thread_rng());

        let commitment = salt.create_commitment(&board);

        let ship_size = 4;
        let shot = (8, 10);

        let answer = get_shot_answer(ship_size, shot, &board, &salt);

        assert_eq!(
            verify_shot_answer(
                ship_size,
                shot,
                board.get_transformation(),
                answer,
                &commitment
            ),
            ShotResult::Miss
        );
    }
}
